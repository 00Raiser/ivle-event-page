import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';

import LandingPage from '../imports/ui/LandingPage.jsx';
import '../imports/startup/accounts-config.js';

Meteor.startup(() => {
	render(<LandingPage />, document.getElementById('render-target'));
	//render(<EventPopupContainer />, document.getElementById('render-target'));
	//render(<CreateEventContainer />, document.getElementById('render-target'));
});