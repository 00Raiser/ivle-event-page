import React, { Component, PropTypes } from 'react';
import {Row, Grid, Col} from 'react-bootstrap';

export default class EventDetailsRowComponent extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		return(
			<div className="panel-body">
				<table className="table table-hover" border="0">
					<tbody>
						<tr> 
							<td>
								<label className="col-md-3 control-label">
									<div className="pull-right">Title: </div>
								</label>


								<div className="col-md-9">
									<span className="iItem-txt">{this.props.event.EventTitle}</span>
								</div>

							</td>
						</tr>
						<tr>
							<td>
								<label className="col-md-3 control-label">
									<div className="pull-right">Organizer :</div>
								</label>
								<div className="col-md-9">
									<span className="iItem-txt">{this.props.event.EventOrganizer}</span>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label className="col-md-3 control-label">
									<div className="pull-right">Tags :</div>
								</label>
								<div className="col-md-9">
									<span className="iItem-txt">{this.props.event.tags}</span>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label className="col-md-3 control-label">
									<div className="pull-right">Description :</div>
								</label>
								<div className="col-md-9">
									<span className="iItem-txt"> {this.props.event.Description} </span>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label className="col-md-3 control-label">
									<div className="pull-right">Date & Time :</div>
								</label>
								<div className="col-md-9">
									<span className="iItem-txt"> {this.props.event.EventDate}</span>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label className="col-md-3 control-label">
									<div className="pull-right">Venue :</div>
								</label>
								<div className="col-md-9">
									<span className="iItem-txt"> {this.props.event.venue}</span>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label className="col-md-3 control-label">
									<div className="pull-right">Price :</div>
								</label>
								<div className="col-md-9">
									<span className="iItem-txt"> {this.props.event.price}</span>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label className="col-md-3 control-label">
									<div className="pull-right">Agenda :</div>
								</label>
								<div className="col-md-9">
									<span className="iItem-txt"> {this.props.event.agenda}</span>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label className="col-md-3 control-label">
									<div className="pull-right">Contact :</div>
								</label>
								<div className="col-md-9">
									<span className="iItem-txt"> {this.props.event.contact}</span>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label className="col-md-3 control-label">
									<div className="pull-right">Hits :</div>
								</label>
								<div className="col-md-9">
									<span className="iItem-txt"> {this.props.event.hits}</span>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			);
	}
}

EventDetailsRowComponent.PropTypes = {
	event: PropTypes.object.isRequired,
}