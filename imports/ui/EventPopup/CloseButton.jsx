import React, { Component, PropTypes } from 'react';

export default class CloseButton extends Component {

	constructor(props) {
		super(props);

	}
	
	handleOnClick() {
		this.props.triggerPopup();
	}

	render() {
		return(
			<div className="pull-right">
				<button className="btn btn-primary" onClick={this.handleOnClick.bind(this)} type="button"> 
					<span className="glyphicon glyphicon-remove"></span>
					CLOSE
				</button>
			</div>

			);
	}
}