import React, { Component, PropTypes } from 'react';

import CloseButton from './CloseButton.jsx';
import EventDetailsRowComponent from './EventDetailsRowComponent.jsx';

export default class EventPopupContainer extends Component {

	constructor(props) {
		super(props);

	}
	handleOnClick() {
		this.props.triggerPopup();
	}

	render() {
		return(
			<div className="col-md-12">
				<div className = "panel panel-default">
					<div className="panel-heading">
						<button className="close" onClick={this.handleOnClick.bind(this)} aria-hidden="true" type="button">×</button>
						<h4>
							<span className="popupBannerHeaderBottom-txt">Student Events</span>
						</h4>
					</div>
					<div className = "panel-body">
						<EventDetailsRowComponent event={this.props.event}/>
						<hr />
						<CloseButton 
							triggerPopup={this.props.triggerPopup} 
						/>
					</div>
				</div>
			</div>

			);
	}
}

EventPopupContainer.PropTypes = {
	event: PropTypes.object.isRequired,
}