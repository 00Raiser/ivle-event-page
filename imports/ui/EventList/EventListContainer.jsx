import React, { Component, PropTypes } from 'react';
import { _ } from 'meteor/erasaur:meteor-lodash';
import { createContainer } from 'meteor/react-meteor-data';
import { Events } from '../../api/events.js';

import CreateEventButton from './CreateEventButton.jsx';
import SearchBar from './SearchBar.jsx';
import EventCounter from './EventCounter.jsx';
import SortableEventListTable from './SortableEventListTable.jsx';


export default class EventListContainer extends Component {

	constructor(props) {
		super(props);

		this.state = {
			toSearch: false,
			searchText: '',
			filteredEvents: {}
		};
	}

	search(toSearch, searchText) {
		this.setState({toSearch: toSearch, searchText: searchText});
	}

	getEvents(){

		 let filteredEvents = this.props.events;

		 if (this.state.toSearch == true) {
		 	var searchString = this.state.searchText;

		 	filteredEvents = _.filter(filteredEvents, function(o) { 
		 		if (_.includes(o.EventTitle,searchString)) {
		 			return true;
				}
				else if (_.includes(o.EventOrganizer,searchString)) {
		 			return true;
		 		} else {
		 			return false;
		 		}
		 	});
		 }

		 this.state.filteredEvents = filteredEvents;

		return filteredEvents.length;
	}


	render() {
		return (
			<div className="tab-content">
						<div className="panel-body">
								<div className="row">
									<div className="col-md-12">
										<div className="col-md-3"><CreateEventButton 
											switchPage={this.props.switchPage} 
										/></div>
										<div className="col-md-6"><SearchBar search={this.search.bind(this)} /></div>
										<div className="col-md-3 text-right form-inline"><EventCounter count={this.getEvents()}/></div>
									</div>
								</div>
								
								
								<table>
									<tbody>
										<tr><td className="height5px"></td></tr>
									</tbody>
								</table>
								<SortableEventListTable 
										events={this.state.filteredEvents}
										setEvent={this.props.setEvent} 
										triggerPopup={this.props.triggerPopup} 
								/>
								
						</div>
			</div>
			);
	}
}

export default createContainer(() => {
  return {
    events: Events.find({}).fetch(),
  };
}, EventListContainer);


