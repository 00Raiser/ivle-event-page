import React, { Component, PropTypes } from 'react';

export default class EventHeader extends Component {

	constructor(props) {
		super(props);


	}

	render() {
		return (
			<tr>
				<th className="width40px text-center" scope="col"> View</th>
				<th scope="col"><a onClick={this.eventTitleHandle.bind(this)}> Event Title </a></th>
				<th scope="col"><a onClick={this.eventOrgHandle.bind(this)}> Event Organizer </a></th>
				<th scope="col">Event Date</th>
			</tr>
			);
	}

	eventTitleHandle() {
		 this.props.changeOrder('EventTitle');
	}

	eventOrgHandle() {
		this.props.changeOrder('EventOrganizer');
	}
}

EventHeader.PropTypes = {
	changeOrder: PropTypes.func.isRequired,
	sortBy: PropTypes.string.isRequired,
}