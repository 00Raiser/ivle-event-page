import React, { Component, PropTypes } from 'react';

import Event from './Event.jsx';
import EventHeader from './EventHeader.jsx'
import { createContainer } from 'meteor/react-meteor-data';

import { Events } from '../../api/events.js';
import { _ } from 'meteor/erasaur:meteor-lodash';

export default class EventRow extends Component {

	constructor(props) {
		super(props);

		this.state = {
			sortBy: 'none',
			titleOrder: -1,
			orgOrder: -1,
		}
	}

	changeOrder(sortBy) {

		if (sortBy == 'EventTitle') {
			var order = this.state.titleOrder*-1;
			this.setState({sortBy: sortBy, titleOrder: order});
		} else if (sortBy == 'EventOrganizer') {
			var order = this.state.orgOrder*-1;
			this.setState({sortBy: sortBy, orgOrder: order});
		}
	};

	renderEvents(){
		 let filteredEvents = this.props.events;

			 if (this.state.sortBy == 'EventTitle' && this.state.titleOrder == 1) {
			 	filteredEvents = _.orderBy(filteredEvents, ['EventTitle'], ['asc']);
			 } else if (this.state.sortBy == 'EventTitle' && this.state.titleOrder == -1) {
			 	filteredEvents = _.orderBy(filteredEvents, ['EventTitle'], ['desc']);
			 } else if (this.state.sortBy == 'EventOrganizer' && this.state.orgOrder == 1) {
			 	filteredEvents = _.orderBy(filteredEvents, ['EventOrganizer'], ['asc']);
			 } else if (this.state.sortBy == 'EventOrganizer' && this.state.orgOrder == -1) {
			 	filteredEvents = _.orderBy(filteredEvents, ['EventOrganizer'], ['desc']);
			 }

		return filteredEvents.map((event) => (
			<Event 
			key={event._id} 
			event = {event} 
			setEvent={this.props.setEvent} 
			triggerPopup={this.props.triggerPopup} 
		/>));
	}


	render() {
			return (
				<tbody>
					<EventHeader sortBy={this.state.sortBy} changeOrder={this.changeOrder.bind(this)}/>
					{this.renderEvents()}
				</tbody>

				);
	}
}

EventRow.propTypes = {
  events: PropTypes.array.isRequired,
};
