import React, { Component, PropTypes } from 'react';


export default class Event extends Component {

	constructor(props) {
		super(props);
	}
	
	handleClick(event) {
		event.preventDefault();
		this.props.triggerPopup();
		
		this.props.setEvent(this.props.event);
	}

	render() {

		return (
				<tr>
					<td className="text-center">
						<a onClick={this.handleClick.bind(this)}><span className="glyphicon glyphicon-eye-open black"></span></a>
					</td>
					<td>{this.props.event.EventTitle}</td>
					<td>{this.props.event.EventOrganizer}</td>
					<td>{this.props.event.EventDate}</td>
				</tr>

			);
	}
}

Event.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  event: PropTypes.object.isRequired,
};