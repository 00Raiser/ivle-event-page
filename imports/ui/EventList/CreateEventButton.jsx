import React, { Component, PropTypes } from 'react';

export default class CreateEventButton extends Component {

	constructor(props) {
		super(props);
	}
	
	createEvent() {
		this.props.switchPage();
	}

	render() {
		return (
				<button className="btn btn-success" type="button" onClick={this.createEvent.bind(this)}>
					<span className="glyphicon glyphicon-plus"></span>
					Create Event 
				</button>
			);
	}
}