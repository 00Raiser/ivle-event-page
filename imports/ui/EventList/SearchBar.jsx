import React, { Component, PropTypes } from 'react';
import { Glyphicon } from 'react-bootstrap';

export default class SearchBar extends Component {
	constructor(props) {
		super(props);

		this.state = {
			searchText: ""
		}
	}

	updateSearch(event) {
		this.setState({searchText: event.target.value});
	}

	startSearch(event) {

		this.props.search(true, this.state.searchText);

		event.preventDefault();
	}

	undoSearch(event) {
		this.setState({searchText: ''});
		this.props.search(false, '');
	}

	render() {
		return (
			<div className = "input-group">
				<form onSubmit={this.startSearch.bind(this)}>
					<input className = "form-control" type = "text" placeholder = "Search" value = {this.state.searchText} onChange={this.updateSearch.bind(this)} />
				</form>
				<span className = "input-group-btn">
					<form>
					<a className = "btn btn-default" onClick={this.startSearch.bind(this)}>
						<span className  = "glyphicon glyphicon-search">
						</span>
					</a>
					<a className = "btn btn-default" onClick={this.undoSearch.bind(this)}>
						<Glyphicon glyph="repeat" className="flip"/>
					</a>
					</form>
				</span>
			</div>
			);
	}
}

SearchBar.PropTypes = {
	search: PropTypes.func.isRequired,
}