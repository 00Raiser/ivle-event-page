import React, { Component, PropTypes } from 'react';

export default class EventCounter extends Component {

	constructor(props) {
		super(props);

	}

	render() {
		return (
			<div>
				Total {this.props.count} items
			</div>
			);
	}
}

EventCounter.PropTypes = {
	count: PropTypes.number.isRequired,
}
