import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Events } from '../../api/events.js';

import EventRow from './EventRow.jsx';
import EventHeader from './EventHeader.jsx';

export default class SortableEventListTable extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		return (
			<table className="table table-hover table-striped">
				<EventRow 
					events={this.props.events}
					setEvent={this.props.setEvent} 
					triggerPopup={this.props.triggerPopup} 
			/>
			</table>
			);
	}
}