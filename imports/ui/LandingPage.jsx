import React, { Component, PropTypes } from 'react';

import CreateEventContainer from './CreateEvent/CreateEventContainer.jsx';
import EventListContainer from './EventList/EventListContainer.jsx';
import EventPopupContainer from './EventPopup/EventPopupContainer.jsx';
import AccountsUIWrapper from './AccountsUIWrapper.jsx';

export default class LandingPage extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			listPage : true,
			popup : false,
			event : {}
		}
	}
	
	switchPage() {
		this.setState({ listPage: !this.state.listPage });
	}
	
	triggerPopup() {
		this.setState({ popup: !this.state.popup });
	}
	
	setEvent(e) {
		this.setState({ event: e });
	}
	
	choosePage() {
		if (this.state.popup) {
			return <EventPopupContainer 
				event={this.state.event}
				triggerPopup={this.triggerPopup.bind(this)} 
			/>;
		}
		
		if (this.state.listPage) {
			return (
				<div>
				<AccountsUIWrapper />
				<EventListContainer 
				switchPage={this.switchPage.bind(this)} 
				setEvent={this.setEvent.bind(this)} 
				triggerPopup={this.triggerPopup.bind(this)} 
			/>
			</div>);
		} else {
			return <CreateEventContainer 
				switchPage={this.switchPage.bind(this)} 
			/>;
		}
	}
	
	render() {
		return (
			<div>{this.choosePage()}</div>
		);
	}
}