import React, { Component, PropTypes } from 'react';

import GeneralTabComponent from './GeneralTabComponent.jsx';
import EventDetailsTabComponent from './EventDetailsTabComponent.jsx';

import { Glyphicon, Panel, PanelGroup, Accordion } from 'react-bootstrap';
import '../../stylesheets/createEvent.css';
import '../../stylesheets/main.css';

import { Events } from '../../api/events.js';

export default class CreateEventContainer extends Component {
	constructor(props) {
		super(props);
		
		this.committeeOptions = [
			"NUSSU", 
			"Faculty Clubs", 
			"Halls of Residences", 
			"Clubs and Societies", 
			"NUS",
			"Interest Groups",
			"Others"
		];
		
		this.categoryOptions = [
			"Bashes",
			"Bazaars",
			"Competitions/Tournament", 
			"Sports and Recreation", 
			"Performances",
			"Announcements",
			"Excursions",
			"Exhibitions",
			"Courses/Workshops",
			"Recruitment",
			"Administration",
			"Charity",
			"Others"
		];
		
		this.displayStartInfo = "One week processing time.";
		this.displayEndInfo = "Max 14 days or 1 day after event.";
		
		this.state = {
			eventTitle: '',
			organiser: '',
			committee: this.committeeOptions[0],
			category: this.categoryOptions[0],
			tags: '',
			displayStart: new Date(),
			displayEnd: new Date(),
			dateTime: '',
			venue: '',
			price: '',
			agenda: '',
			contact: '',
			description: ''
		};
  	}
	
	initialise() {
		this.setState({
			eventTitle: '',
			organiser: '',
			committee: this.committeeOptions[0],
			category: this.categoryOptions[0],
			tags: '',
			displayStart: new Date().toISOString(),
			displayEnd: new Date().toISOString(),
			dateTime: '',
			venue: '',
			price: '',
			agenda: '',
			contact: '',
			description: ''
		});
	}
	
	// VALIDATE
	
	validateEventTitle(input) {
		if (input.length>200) {
			return "Please enter text less than 200 characters";
		} else if (input.length==0) {
			return "Event Title cannot be empty";
		} else {
			return "";
		}
	}
	
	validateOrganiser(input) {
		if (input.length>200) {
			return "Please enter text less than 100 characters";
		} else if (input.length==0) {
			return "Organizer cannot be empty";
		} else {
			return '';
		}
	}
	
	validateCommittee(input) {
		return '';
	}
	
	validateCategory(input) {
		return '';
	}
	
	validateTags(input) {
		if (input.length>255) {
			return "Please enter text less than 255 characters";			
		}
		return '';
	}
	
	validateDisplayStart(input) {
		// nothing to validate because date picker implemented
		return '';
	}
	
	validateDisplayEnd(input) {
		// nothing to validate because date picker implemented
		return '';
	}
	
	validateDescription(input) {
		return '';
	}
	
	validateDateTime(input) {
		if (input.length>500) {
			return "Event date and time allow maximum 500 chars.";
		} else {
			return '';
		}
	}
	
	validateVenue(input) {
		if (input.length>500) {
			return "Event venue allow maximum 500 chars."
		} else {
			return '';
		}
	}
	
	validatePrice(input) {
		if (input.length>500) {
			return "Event price allow maximum 500 chars."
		} else {
			return '';
		}
	}
	
	validateAgenda(input) {
		if (input.length>500) {
			return "Event agenda allow maximum 500 chars."
		} else {
			return '';
		}
	}
	
	validateContact(input) {
		if (input.length>500) {
			return "Event contact allow maximum 500 chars."
		} else {
			return '';
		}
	}
	
	
	// SETTERS
	
	setEventTitle(input) {
		this.setState({ eventTitle: input });
		return this.validateEventTitle(input);
	}
	
	setOrganiser(input) {
		this.setState({organiser: input });
		return this.validateOrganiser(input);
	}
	
	setCommittee(input) {
		this.setState({committee: input });
		return this.validateCommittee(input);
	}
	
	setCategory(input) {
		this.setState({category: input });
		return this.validateCategory(input);
	}
	
	setTags(input) {
		this.setState({tags: input });
		return this.validateTags(input);
	}
	
	setDisplayStart(input) {
		this.setState({displayStart: input });
		return this.validateDisplayStart(input);
	}
	
	setDisplayEnd(input) {
		this.setState({displayEnd: input });
		return this.validateDisplayEnd(input);
	}
	
	setDescription(input) {
		this.setState({description: input });
		return this.validateDescription(input);
	}
	
	setDateTime(input) {
		this.setState({dateTime: input });
		return this.validateDateTime(input);
	}
	
	setVenue(input) {
		this.setState({ venue: input });
		return this.validateVenue(input);
	}
	
	setPrice(input) {
		this.setState({ price: input });
		return this.validatePrice(input);
	}
	
	setAgenda(input) {
		this.setState({ agenda: input });
		return this.validateAgenda(input);
	}
	
	setContact(input) {
		this.setState({ contact: input });
		return this.validateContact(input);
	}
	
	handleSubmit(event) {
		event.preventDefault();
		let object = this.state;
		
		let validators = {
			eventTitle: this.validateEventTitle,
			organiser: this.validateOrganiser,
			committee: this.validateCommittee,
			category: this.validateCategory,
			tags: this.validateTags,
			displayStart: this.validateDisplayStart,
			displayEnd: this.validateDisplayEnd,
			description: this.validateDescription,

			dateTime: this.validateDateTime,
			venue: this.validateVenue,
			price: this.validatePrice,
			agenda: this.validateAgenda,
			contact: this.validateContact
		};
		
		for (let property in object) {
			if (object.hasOwnProperty(property)) {
				let error = validators[property](object[property]);
				if (error!='') {
					alert(error + "\nPlease input the fields properly!");
					return '';
				}
				if (object[property]=='') {
					object[property] = '--';
				}
			}
		}
		
		Events.insert({
			EventTitle: object.eventTitle,
			EventOrganizer: object.organiser,
			committee: object.committee,
			category: object.category,
			tags: object.tags,
			displayStart: object.displayStart,
			displayEnd: object.displayEnd,
			description: object.description,

			EventDate: object.dateTime,
			venue: object.venue,
			price: object.price,
			agenda: object.agenda,
			contact: object.contact
		});
		this.handleCancel(event); // calls cancel to return back to home page
	}
	
	handleCancel(event) {
		event.preventDefault();
		this.initialise();
	
		// reroute back to original page
		this.props.switchPage();
	}
	
	render() {
		return (
			<div className="frame">
				<div className="row">
					<div className="panel-group">
						<div className="panel panel-default">
							<div className="panel-heading">
								<h4 className="panel-title">
									<a className="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> General</a>
								</h4>
							</div>
							<div id="collapseOne" className="panel-collapse collapse in">
								<div className="panel-body">
									<GeneralTabComponent 
										eventTitle={this.state.eventTitle} 
										organiser={this.state.organiser} 
										committee={this.state.committee} 
										category={this.state.category} 
										tags={this.state.tags} 
										displayStart={this.state.displayStart} 
										displayEnd={this.state.displayEnd} 
										description={this.state.description} 
			
										setEventTitle={this.setEventTitle.bind(this)} 
										setOrganiser={this.setOrganiser.bind(this)} 
										setCommittee={this.setCommittee.bind(this)} 
										setCategory={this.setCategory.bind(this)} 
										setTags={this.setTags.bind(this)} 
										setDisplayStart={this.setDisplayStart.bind(this)} 
										setDisplayEnd={this.setDisplayEnd.bind(this)} 
										setDescription={this.setDescription.bind(this)} 
										
										displayStartInfo={this.displayStartInfo} 
										displayEndInfo={this.displayEndInfo} 
										committeeOptions={this.committeeOptions} 
										categoryOptions={this.categoryOptions} 
									/>
								</div>
							</div>
						</div>
						<div className="panel panel-default">
							<div className="panel-heading">
								<h4 className="panel-title">
									<a className="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> Event Details</a>
								</h4>
							</div>
							<div id="collapseTwo" className="panel-collapse collapse in">
								<div className="panel-body">
									<EventDetailsTabComponent 
										dateTime={this.state.dateTime} 
										venue={this.state.venue} 
										price={this.state.price} 
										agenda={this.state.agenda} 
										contact={this.state.contact} 
										
										setDateTime={this.setDateTime.bind(this)} 
										setVenue={this.setVenue.bind(this)} 
										setPrice={this.setPrice.bind(this)} 
										setAgenda={this.setAgenda.bind(this)} 
										setContact={this.setContact.bind(this)} 
									/>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			
			
				<div className="row"><div className="text-right">
            		<button onClick={this.handleCancel.bind(this)}  id="ctl00_ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_btnCancel" type="button" class="btn btn-default" data-loading-text="Cancel...">
                		<span className="glyphicon glyphicon-remove-circle"></span>
                		Cancel 
            		</button> 

            		<button onClick={this.handleSubmit.bind(this)}  id="ctl00_ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_btnSubmit" type="button" className="btn btn-primary" data-loading-text="Saving...">
						<span className="glyphicon glyphicon-floppy-save"></span>
                		Save
            		</button>
				</div></div>
			</div>
		);
	}
}