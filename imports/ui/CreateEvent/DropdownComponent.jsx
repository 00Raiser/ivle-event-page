import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';

export default class DropdownComponent extends Component {
	constructor(props) {
		super(props);
	}
	
	handleChange(event){
		this.props.setContent(event.target.value);
	}
	
	renderOptions() {
		return this.props.options.map((option) => (
			<option value={option}>{option}</option>));
	}
	
	render() {
		return (
			<td>
				<label id="lcUserType" className="col-md-3 text-right">{this.props.title}</label>
				<div className="col-md-9">
					<select onChange={this.handleChange.bind(this)}>
						{this.renderOptions()}
					</select>
				</div>
			</td>
		);
	}
}