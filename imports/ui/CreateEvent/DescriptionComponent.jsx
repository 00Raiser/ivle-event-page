import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';

export default class DescriptionComponent extends Component {
	constructor(props) {
		super(props);
		
		this.red = {
			color: 'red'
		}
		this.error = '';
	}
	
	tagComponent() {
		if (this.props.tagged) {
			return (<a id="aTags" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true" data-content="A tag is a <b>personal single word</b> identification that best describes the event. You can have multiple tags <u>separated with a comma</u> to best describe the event (e.g. bash,bazaar,club).<br>Tags will make it easier for students to search for the event. The maximum length for <u>each tag</u> is 25 characters." href="javascript:return false;"><span className="glyphicon glyphicon-info-sign"></span></a>);
		} else if (this.props.compulsory) {
			return (<span style={this.red}>*</span>);
		} else {
			return '';
		}
	}
	
	handleSubmit(event){
		event.preventDefault();
	}
	
	handleChange(){
		this.error = this.props.setContent(
			ReactDOM.findDOMNode(this.refs.textInput).value.trim()
		);
	}
	
	displayError() {
		return this.error;
	}
	
	render() {
		return (
			<td>
				<label id="lcUserType" className="col-md-3 text-right">{this.tagComponent()} {this.props.title}</label>		     
				<div className="col-md-9">
					<textarea 
						rows="2" 
						cols="20"
						className="form-control" 
						id="ctl00_ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_evtdesp"
					></textarea>
				</div>
			</td>
		);
	}
}