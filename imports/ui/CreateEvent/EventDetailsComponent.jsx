import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';

export default class EventDetailsTitleComponent extends Component {
	constructor(props) {
		super(props);
		
		this.red = {
			color: 'red'
		}
		
		this.style = {
			height: '50px'
		}
		this.error = '';
		this.count = 0;
	}
	
	handleSubmit(event){
		event.preventDefault();
	}
	
	handleChange(){
		const text = ReactDOM.findDOMNode(this.refs.textInput).value;
		this.count = text.length;
		this.error = this.props.setContent(	text );
	}
	
	displayError() {
		return this.error;
	}
	
	render() {
		return (
			<td>
				<label id="Label20" className="col-md-3 text-right">{this.props.title}</label>		     
				<div className="col-md-8">
					<form onSubmit={this.handleSubmit.bind(this)}> 
						<textarea 
							ref="textInput" 
							value={this.props.content} 
							onChange={this.handleChange.bind(this)} 
							className="form-control" 
							placeholder={this.props.placeholder} 
							rows="2" 
							cols="20"
							style={this.style} 
							id="ctl00_ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_evtTitle"	
						></textarea><br />
					</form>	
					<span className="font-error" style={this.red}>{this.displayError}</span>
				</div>
					
				<div class="col-md-1">
                	<span id="ctl00_ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_Label1" class="iName-txt">Char. Count: {this.count}</span>
				</div>
			</td>
		);
	}
}