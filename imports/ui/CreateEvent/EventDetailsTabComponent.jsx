import React, { Component, PropTypes } from 'react';
import { Glyphicon, Panel, PanelGroup, Accordion } from 'react-bootstrap';
import EventDetailsComponent from './EventDetailsComponent.jsx';

export default class EventDetailsTabComponent extends Component {
	constructor(props) {
		super(props);
	}
	
	render() {
		return (
			<table className="table table-hover">
				<tr>
					<EventDetailsComponent 
									title="Date & Time" 
									content={this.props.dateTime} 
									setContent={this.props.setDateTime} 
									placeholder="Specify the actual event schedule here." />
				</tr>
				<tr>
					<EventDetailsComponent
									title="Venue" 
									content={this.props.venue} 
									setContent={this.props.setVenue} />
				</tr>
				<tr>
					<EventDetailsComponent
									title="Price" 
									content={this.props.price} 
									setContent={this.props.setPrice} />
				</tr>
				<tr>
					<EventDetailsComponent
									title="Agenda" 
									content={this.props.agenda} 
									setContent={this.props.setAgenda} />
				</tr>
				<tr>
					<EventDetailsComponent
									title="Contact" 
									content={this.props.contact} 
									setContent={this.props.setContact} />
				</tr>
			</table>
		);
	}
}