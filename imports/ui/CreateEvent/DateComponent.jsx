import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { OverlayTrigger, Popover } from 'react-bootstrap';

export default class DateComponent extends Component {
	constructor(props) {
		super(props);
	}
	
	handleChange(){
		this.props.setDate(
			ReactDOM.findDOMNode(this.refs.dateInput).value.trim()
		);
	}
	
	content() {
		return (<Popover>{this.props.dateInfo}</Popover>); 
	}
	
	infoComponent() {
		if (this.props.dateInfo!=undefined) {
			return (
				<OverlayTrigger trigger="hover" 
					id="aTags" 
					placement="bottom" 
					overlay={this.content()} 
					>
					<span className="glyphicon glyphicon-info-sign"></span> 
				</OverlayTrigger>
			);
		}
	}
	
	render() {
		return (
			<td>
				<label ID="Label16" className="col-md-3 text-right">{this.infoComponent()} {this.props.title}</label>		
				<div className="col-md-3">
					<div className='input-group date' id='dtp_ctl00_ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_evtStart'>
						<input 
							type='text' 
							ref="dateInput" 
							value={this.props.date} 
							onChange={this.handleChange.bind(this)} 
							className="form-control" id='txt_dtp_ctl00_ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_evtStart' 
							data-date-format="DD-MM-YYYY hh:mm A" 
						/>
						<span className="input-group-addon">
							<OverlayTrigger trigger="hover" 
								id="aTags" 
								placement="bottom" 
								overlay={<Popover>Format: DD-MM-YYYY hh:mm AM/PM</Popover>} 
							>
							<span className="glyphicon glyphicon-time"></span> 
							</OverlayTrigger>
						</span>
					</div>
				</div>
            </td>
		);
	}
}