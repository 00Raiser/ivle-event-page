import React, { Component, PropTypes } from 'react';
import { Glyphicon, Panel, PanelGroup, Accordion } from 'react-bootstrap';
import TitleComponent from './TitleComponent.jsx';
import DropdownComponent from './DropdownComponent.jsx';
import DateComponent from './DateComponent.jsx';
import DescriptionComponent from './DescriptionComponent.jsx';

export default class GeneralTabComponent extends Component {
	constructor(props) {
		super(props);
	}
	
	render() {
		return (
			<table className="table table-hover">
				<tr>
					<TitleComponent
									title="Event Title" 
									content={this.props.eventTitle} 
									setContent={this.props.setEventTitle} 
									compulsory="true" 
					/>
				</tr>
				<tr>
					<TitleComponent
									title="Organiser" 
									content={this.props.organiser} 
									setContent={this.props.setOrganiser} 
									compulsory="true" 
					/>
				</tr>
				<tr>
					<DropdownComponent
									title="Committee" 
									content={this.props.committee} 
									setContent={this.props.setCommittee} 
									options={this.props.committeeOptions} 
					/>
				</tr>
				<tr>
					<DropdownComponent
									title="Category" 
									content={this.props.category} 
									setContent={this.props.setCategory} 
									options={this.props.categoryOptions} 
					/>
				</tr>
				<tr>
					<TitleComponent
									title="Tags" 
									content={this.props.tags } 
									setContent={this.props.setTags } 
									tagged="true" 
					/>
				</tr>
				<tr>
					<DateComponent
									title="Display Start" 
									date={this.props.displayStart } 
									setDate={this.props.setDisplayStart } 
									dateInfo={this.props.displayStartInfo} 
					/>
				</tr>
				<tr>
					<DateComponent
									title="Display End" 
									date={this.props.displayEnd } 
									setDate={this.props.setDisplayEnd } 
									dateInfo={this.props.displayEndInfo} 
					/>
				</tr>
				<tr>
					<DescriptionComponent
									title="Description" 
									content={this.props.description } 
									setContent={this.props.setDescription } 
					/>
				</tr>
			</table>
		);
	}
}