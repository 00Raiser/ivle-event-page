import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { OverlayTrigger, Popover } from 'react-bootstrap';

export default class TitleComponent extends Component {
	constructor(props) {
		super(props);
		
		this.red = {
			color: 'red'
		}
		this.error = '';
	}
	
	tagContent() {
		return (<Popover>A tag is a <b>personal single word</b> identification that best describes the event. You can have multiple tags <u>separated with a comma</u> to best describe the event (e.g. bash,bazaar,club).<br />Tags will make it easier for students to search for the event. The maximum length for <u>each tag</u> is 25 characters.</Popover>); 
	}
	
	tagComponent() {
		if (this.props.tagged) {
			return (
				<OverlayTrigger trigger="hover" 
					id="aTags" 
					placement="bottom" 
					overlay={this.tagContent()} 
					>
					<span className="glyphicon glyphicon-info-sign"></span>
				</OverlayTrigger>
			);
		} else if (this.props.compulsory) {
			return (<span style={this.red}>*</span>);
		} else {
			return '';
		}
	}
	
	handleSubmit(event){
		event.preventDefault();
	}
	
	handleChange(){
		this.error = this.props.setContent(
			ReactDOM.findDOMNode(this.refs.textInput).value.trim()
		);
	}
	
	displayError() {
		return this.error;
	}
	
	render() {
		return (
			<td>
				<label id="lcUserType" className="col-md-3 text-right">{this.tagComponent()} {this.props.title}</label>		     
				<div className="col-md-9">
					<form onSubmit={this.handleSubmit.bind(this)}> 
						<input 
							type="text" 
							ref="textInput" 
							value={this.props.content} 
							onChange={this.handleChange.bind(this)} 
							className="form-control" 
							placeholder={this.props.placeholder} 
							id="ctl00_ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_evtTitle"	
						/>
					</form>	
					<span className="font-error" style={this.red}>{this.displayError()}</span>
				</div>
			</td>
		);
	}
}