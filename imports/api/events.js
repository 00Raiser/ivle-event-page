import { Mongo } from 'meteor/mongo';
 
export const Events = new Mongo.Collection('events');

/*
2 objects inside
General {
	eventTitle,
	organiser,
	committee,
	category,
	tags,
	displayStart,
	displayEnd,
	description
}

Details {
	dateTime,
	venue,
	price,
	agenda
}
//*/