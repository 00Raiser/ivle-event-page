/* eslint-env mocha */
 
import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { assert } from 'meteor/practicalmeteor:chai';
import { Events } from './events.js';
 
if (Meteor.isServer) {
  describe('Events', () => {
    describe('methods', () => {

      beforeEach(() => {
        Events.remove({});
        taskId = Events.insert({

          eventTitle: 'test event',
        });
      });

      it('can get events from db', () => {

        assert.equal(Events.find().count(), 1);
      });
    });
  });
}